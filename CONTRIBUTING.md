# Contributing

 > PLEASE DON'T PUSH TO `master`

### Cloning and branching
 1. Clone repository with `git clone https://gitlab.com/DeloitteTipu/tipu_client` and enter your GitLab credentials
 2. Run `git checkout -b *branch_name*` entering an appropriate branch_name. This command will branch and checkout into the new branch.
 3. Enter repositiry and execute the command `npm install` to install node dependancies
 4. Run `ng serve -o` to serve the project
 5. DONE