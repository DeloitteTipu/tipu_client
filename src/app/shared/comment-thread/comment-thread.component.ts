import {Component, Input, OnInit} from '@angular/core';
import {CommentTarget} from '../../structs/CommentTarget';
import {Comment} from '../../structs/Comment';
import {SessionDataService} from '../../service/session-data.service';
import {TipuAPIService} from '../../service/tipuapi.service';

@Component({
  selector: 'app-comment-thread',
  templateUrl: './comment-thread.component.html',
  styleUrls: ['./comment-thread.component.scss']
})
export class CommentThreadComponent implements OnInit {

  @Input() targetID: number;
  @Input() targetType: CommentTarget;
  comments: Comment[];

  constructor(private sessionData: SessionDataService) { }

  ngOnInit() {
    this.loadComments();
  }

  loadComments() {
    this.sessionData.tipuAPI.getComments(this.targetType, this.targetID, {}).subscribe((comments) => {
      this.comments = comments;
    });
  }

  deleteComment(commentID: number) {
    this.sessionData.tipuAPI.deleteComment(this.targetType, this.targetID, commentID).subscribe((data) => {
      this.loadComments();
    });
  }

  updateComment(commentID: number, newComment: string) {
    this.sessionData.tipuAPI.updateComment(this.targetType, this.targetID, commentID, {content: newComment}).subscribe((data) => {
      this.loadComments();
    });
  }

  getReplies(commentID: number) {
    if (this.comments) {
      return this.comments.filter(c => c.replying_to === commentID);
    }
  }

  getMainComments() {
    if (this.comments) {
      return this.comments.filter(c => c.replying_to === null);

    }
  }

  createComment(comment, replyToID?: string) {
    const details = {
      content: comment.value,
      commenter_id: this.sessionData.currentUser.id
    };

    if (replyToID) {
      details['replying_to'] = replyToID;
    }

    this.sessionData.tipuAPI.createComment(this.targetType, this.targetID,
      details).subscribe((data) => {
        console.log(data);
      comment.value = '';
      this.loadComments();
    });
  }

}
