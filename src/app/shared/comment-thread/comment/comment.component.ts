import {Component, EventEmitter, Input, OnInit, Output, TemplateRef} from '@angular/core';
import {Comment} from '../../../structs/Comment';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {SessionDataService} from '../../../service/session-data.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  @Input() comment: Comment;
  @Input() replies: Comment[];
  @Output() delete: EventEmitter<Comment> = new EventEmitter<Comment>();
  @Output() update: EventEmitter<object> = new EventEmitter<object>();
  @Output() reply: EventEmitter<object> = new EventEmitter<object>();
  modalRef: BsModalRef;

  constructor(private sessionData: SessionDataService, private modalService: BsModalService) { }

  ngOnInit() {
  }

  openTemplateInModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }


  editComment(comment: Comment, newComment: string) {
    this.update.emit({id: comment.id, content: newComment});
    this.closeModal();
  }

  deleteComment(comment: Comment) {
    this.delete.emit(comment);
    this.closeModal();
  }

  createReply(newReply) {
    this.reply.emit({id: this.comment.id, content: newReply});
  }

  editReply() {

  }


  closeModal() {
    this.modalRef.hide();
  }

}
