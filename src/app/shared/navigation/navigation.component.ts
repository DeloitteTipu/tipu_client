import { Component, OnInit } from '@angular/core';
import {Router, RouterModule} from '@angular/router';
import {SessionDataService} from '../../service/session-data.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  active: string;

  constructor(private sessionData: SessionDataService, private router: Router) { }

  ngOnInit() {
  }

  updateActive() {
    if (this.router !== null) {
      this.active = this.router.url;

    }
  }

  isActive(url: string) {
    return this.active !== null ? this.active === url : false;
  }

  logout() {
    this.sessionData.setCurrentUser(null);
    this.router.navigate(['/login']);
  }

}
