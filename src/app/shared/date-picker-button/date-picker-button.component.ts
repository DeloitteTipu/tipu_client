import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-date-picker-button',
  templateUrl: './date-picker-button.component.html',
  styleUrls: ['./date-picker-button.component.scss']
})
export class DatePickerButtonComponent implements OnInit {

  open = false;
  @Input() textWhileDisabled: string;
  @Input() textWhileEnabled: string;
  @Input() dateString: string;
  date: Date;

  @Output() update = new EventEmitter<{open: boolean, date: Date}>();

  constructor() {
  }

  ngOnInit() {
    if (this.dateString) {
      this.date = new Date(this.dateString);
      this.open = true;
    } else {
      this.date = new Date();
    }
    console.log(this.date);
  }

  emitUpdate(date) {
    // this.date.setDate(this.date.getDate() + 1);
    this.update.emit({open: this.open, date: date});
  }


}
