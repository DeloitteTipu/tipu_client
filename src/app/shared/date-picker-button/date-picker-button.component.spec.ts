import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatePickerButtonComponent } from './date-picker-button.component';

describe('DatePickerButtonComponent', () => {
  let component: DatePickerButtonComponent;
  let fixture: ComponentFixture<DatePickerButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatePickerButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatePickerButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
