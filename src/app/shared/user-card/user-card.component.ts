import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {User} from '../../structs/User';

import {TipuAPIService} from '../../service/tipuapi.service';
import {Observable} from 'rxjs/Observable';
import {SessionDataService} from '../../service/session-data.service';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit, OnDestroy {

  constructor(private tipuAPI: TipuAPIService, private sessionData: SessionDataService) {


  }

  @Input() currentUserObs: Observable<User>;
  @Input() targetUserObs: Observable<User>;
  currentUser: User;
  targetUser: User;
  targetUsersCoach: User;
  usersCoachees: User[];

  userImageURL;

  @Output() userUpdate: EventEmitter<User> = new EventEmitter();

  ngOnInit() {

    this.sessionData.targetUserObservable().subscribe((user) => {
      this.targetUser = user;
      this.userImageURL = this.sessionData.tipuAPI.getUserImageURL(user.photo_url);
      this.updateCoach();
    });

    this.sessionData.currentUserObservable().subscribe((user) => {
      this.currentUser = user;
      this.updateCoachees();
    });

  }

  ngOnDestroy() {

  }

  updateCoach() {

    /*
      This will returns the details of my coach
     */
    this.tipuAPI.getUsers({id: this.targetUser.coach_id}).subscribe((coachList) => {
      this.targetUsersCoach = coachList[0];
    });

  }

  updateCoachees() {

    if (this.currentUser) {
      this.tipuAPI.getUsers({coach_id: this.currentUser.id}).subscribe((coacheesList) => {
        this.usersCoachees = coacheesList;
      });
    }
  }

  /**
   * This method will be called when the selected user is updated
   */
  userChanged() {
    this.userUpdate.emit(this.targetUser);

  }

  coacheeIsSelected() {
    return this.targetUser !== this.currentUser;
  }

  public formatter(user: User, query?: string): string {
    return user.first_name + ' ' + user.last_name;

  }
}
