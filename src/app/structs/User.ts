export class User {
  id: number;
  first_name: string;
  last_name: string;
  coach_id: number;
  role: string;
  service_area: string;
  photo_url?: string;
  email?: string;
  last_promoted?: Date;


}
