export class Goal {
  user_id: number;
  setter_id: number;
  description: string;
  title: string;

  id?: number;
  date_made?: string;
  date_finished?: string;
  end_date?: string;
  progress?: number;
  complete?: boolean;
  last_update?: string;
}
