export enum CommentTarget {
  user = 'user',
  goal = 'goal',
  review = 'review'
}
