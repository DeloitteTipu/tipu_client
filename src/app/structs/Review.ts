export class Review {
  id: number;
  user_id: number;
  year: number;
  end_of_year: number;
  user_comment: string;
  coach_comment: string;
  promotion_milestone: string;
  user_rating: number;
  coach_rating: number;
  coach_id: number;
}
