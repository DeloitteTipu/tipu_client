export class Comment {
  id: number;
  target_type: string;
  target_id: number;
  commenter_id: number;
  content: string;
  timestamp: string;
  replying_to?: number;
}
