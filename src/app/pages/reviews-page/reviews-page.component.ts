import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../structs/User';
import {SessionDataService} from '../../service/session-data.service';
import {Subscription} from 'rxjs/Subscription';
import {Review} from '../../structs/Review';
import {Router} from '@angular/router';

@Component({
  selector: 'app-reviews-page',
  templateUrl: './reviews-page.component.html',
  styleUrls: ['./reviews-page.component.scss']
})
export class ReviewsPageComponent implements OnInit, OnDestroy {

  /*
  Store the subscribed user and goal variables and subscription for unsubscription in ngOnDestroy
   */
  currentUser: User;
  currentUserSubscription: Subscription;

  targetUser: User;
  targetUserSubscription: Subscription;

  reviews: Review[];
  reviewsSubscription: Subscription;

  expandedReviews: Set<number> = new Set<number>();

  selectedRank: {name: string, value: number};

  ranks = [
    {name: 'Below Target', value: 0},
    {name: 'On Target', value: 1},
    {name: 'High Performance', value: 2},
    {name: 'Exceptional Performance', value: 3}
  ];

  constructor(private sessionData: SessionDataService, private router: Router) {

    if (!this.sessionData.currentUser) {
      this.router.navigate(['/login']);
    }

    /*
    Add subscriptions to the user and goal observables in the service to recieve updates when the user car
    is updated
     */
    this.currentUser = this.sessionData.currentUser;
    this.currentUserSubscription = this.sessionData.currentUserObservable().subscribe((data) => {
      this.currentUser = data;
    });

    this.targetUser = this.sessionData.targetUser;
    this.targetUserSubscription = this.sessionData.targetUserObservable().subscribe((data) => {
      this.expandedReviews = new Set<number>();
      this.targetUser = data;
    });

    this.reviews = this.sessionData.reviews;
    this.reviewsSubscription = this.sessionData.reviewsObservable().subscribe((data) => {
      this.reviews = data;
    });

    // This line is weird but if I dont update the target user then it doesn't seem to bind properly
    this.sessionData.updateTargetUser(this.targetUser);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    /*
    Remove the subscriptions to the service
     */
    this.currentUserSubscription.unsubscribe();
    this.targetUserSubscription.unsubscribe();
  }

  toggleReview(id: number) {
    if (this.expandedReviews.has(id)) {
      this.closeReview(id);
    } else {
      this.openReview(id);
    }
  }

  openReview(id: number) {
    this.expandedReviews = new Set<number>().add(id);
  }

  closeReview(id: number) {
    this.expandedReviews.delete(id);
  }

  updateCoacheeComment(reviewID: number, comment: string) {
    console.log(this.selectedRank);
    this.sessionData.tipuAPI.updateReview(reviewID, {user_comment: comment, user_rating: this.selectedRank.value}).subscribe((data) => {
      this.sessionData.synchronise();
    });
  }

  updateCoachComment(reviewID: number, comment: string, promotionMilestone: string) {
    this.sessionData.tipuAPI.updateReview(reviewID,
      {
                coach_comment: comment,
                coach_id: this.sessionData.currentUser.id,
                coach_rating: this.selectedRank.value,
                promotion_milestone: promotionMilestone
              }).subscribe((data) => {

      this.sessionData.synchronise();
    });
  }

  public formatter(rank, query?: string): string {
    return rank.name;

  }

}
