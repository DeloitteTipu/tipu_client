import {SessionDataService} from '../../../../service/session-data.service';

export class CoacheeGoalsBar {

  constructor(private sessionData: SessionDataService) {}

  options;

  build() {

    this.sessionData.tipuAPI.getUsers({coach_id: this.sessionData.currentUser.id}).subscribe(coachees => {

      const promises: Promise<any>[] = [];

      for (const coachee of coachees) {
        promises.push(new Promise(resolve => {
          this.sessionData.tipuAPI.getGoalsCompletion(coachee.id).subscribe(data => {
            resolve({data: data, name: coachee.first_name + ' ' + coachee.last_name});
          });
        }));
      }


      Promise.all(promises).then(data => {


        const categories = [];
        const completeData = [];
        const incompleteData = [];

        for (const userData of data) {
          categories.push(userData.name);

          if (userData.data.length === 2) {
            if (userData.data[0].name === 'Complete') {
              completeData.push(userData.data[0].y);
              incompleteData.push(userData.data[1].y);
            } else if (userData.data[0].name === 'Incomplete') {
              completeData.push(userData.data[1].y);
              incompleteData.push(userData.data[0].y);
            }
          } else if (userData.data.length === 1) {
            if (userData.data[0].name === 'Complete') {
              completeData.push(userData.data[0].y);
              incompleteData.push(0);
            } else {
              incompleteData.push(userData.data[0].y);
              completeData.push(0);
            }
          } else {
            incompleteData.push(0);
            completeData.push(0);
          }
        }

        this.options = {
          chart: {
            type: 'bar'
          },
          credits: {
            enabled: false
          },
          title: {
            text: 'Coachee goal completion'
          },
          xAxis: {
            categories: categories
          },
          yAxis: {
            min: 0,
            allowDecimals: false,
            title: {
              text: 'Goals completion split'
            }
          },
          legend: {
            reversed: true
          },
          plotOptions: {
            series: {
              stacking: 'normal'
            }
          },
          series: [{
            name: 'Complete',
            data: completeData
          }, {
            name: 'Incomplete',
            data: incompleteData
          }]
        };
      });
    });
  }
}
