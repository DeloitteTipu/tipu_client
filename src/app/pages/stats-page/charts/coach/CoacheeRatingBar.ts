import {SessionDataService} from '../../../../service/session-data.service';

export class CoacheeRatingBar {

  constructor(private sessionData: SessionDataService) {}

  options;

  build() {
    const promises: Promise<any>[] = [];

    promises.push(new Promise<any>(resolve => {
      this.sessionData.tipuAPI.getAverageRating(2018, 0).subscribe(data => {
        resolve({all: data});
      });
    }));

    promises.push(new Promise<any>(resolve => {
      this.sessionData.tipuAPI.getAverageRating(2018, 0, this.sessionData.currentUser.id).subscribe(data => {
        resolve({coachees: data});
      });
    }));

    Promise.all(promises).then(data => {

      const categories = [];
      const allData = [];
      const coacheeData = [];

      for (const set of data) {
        if ('all' in set) {
          let total = 0;

          for (const group of set.all) {
            total += group.y;
          }

          for (const group of set.all) {
            categories.push(group.name);
            allData.push(Math.round(group.y / total * 100));
          }
        }

        if ('coachees' in set) {

          let total = 0;

          for (const entry of set.coachees) {
            total += entry.y;
          }

          for (const cat of categories) {
            let added = false;
            for (const entry of set.coachees) {
              if (entry.name === cat && total > 0) {
                added = true;
                coacheeData.push(Math.round(entry.y / total * 100));
              }
            }
            if (!added) {
              coacheeData.push(0);
            }
          }
        }
      }

      this.options = {
        chart: {
          type: 'column'
        },
        credits: {
          enabled: false
        },
        title: {
          text: 'Coachee ratings vs all users'
        },
        xAxis: {
          categories: categories
        },
        yAxis: {
          title: {
            text: 'Percentage of people'
          }

        },
        legend: {
          shadow: false
        },
        tooltip: {
          shared: true
        },
        plotOptions: {
          column: {
            grouping: false,
            shadow: false,
            borderWidth: 0
          }
        },
        series: [{
          name: 'All users',
          color: 'rgba(248,161,63,1)',
          data: allData,
          tooltip: {
            valueSuffix: '%'
          },

          pointPadding: 0.1
        }, {
          name: 'Your coacheees',
          color: 'rgba(186,60,61,.9)',
          data: coacheeData,
          dataLabels: {
            enabled: true,
            formatter: function () {
              if (this.y > 0) {
                return this.y + '%';
              } else {
                return null;
              }
            }
          },
          tooltip: {
            valueSuffix: '%'

          },
          pointPadding: 0.25
        }]
      };
    });
  }
}
