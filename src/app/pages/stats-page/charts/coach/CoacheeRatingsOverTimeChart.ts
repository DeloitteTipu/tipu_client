import {SessionDataService} from '../../../../service/session-data.service';
import {promise} from 'selenium-webdriver';

export class CoacheeRatingsOverTimeChart {

  constructor(private sessionData: SessionDataService) {}

  options;

  build() {
    this.sessionData.tipuAPI.getUsers({coach_id: this.sessionData.currentUser.id}).subscribe(coachees => {

      const promises: Promise<any>[] = [];

      for (const coachee of coachees) {
        promises.push(new Promise(resolve => {
          this.sessionData.tipuAPI.getRatingsOverTime(coachee.id).subscribe(data => {
            resolve({data: data, name: coachee.first_name + ' ' + coachee.last_name});
          });
        }));
      }

      Promise.all(promises).then((data: any) => {

        const categories = new Set();
        const series = [];

        for (const info of data) {
          for (const cat of info.data.categories) {
            categories.add(cat);
          }
        }

        const sortedCategories = Array.from(categories);

        sortedCategories.sort((a: string, b: string): number => {
         if (a.split(' ')[0] < b.split(' ')[0]) {
           return -1;
         } else if (a.split(' ')[0] > b.split(' ')[0]) {
           return 1;
         } else {
           if (a.split(' ')[1] < b.split(' ')[1]) {
             return 1;
           }
           if (a.split(' ')[1] > b.split(' ')[1]) {
             return -1;
           }
         }
        });

        for (const info of data) {
          const seriesData = [];

          for (let i = 0; i < sortedCategories.length; i++) {
            if (info.data.categories.includes(sortedCategories[i])) {
              seriesData.push(info.data.data[info.data.categories.indexOf(sortedCategories[i])]);
            } else {

              seriesData.push(null);
            }
          }

          series.push({name: info.name, data: seriesData});
        }

        this.options = {
          chart: {
            type: 'line'
          },
          credits: {
            enabled: false
          },
          title: {
            text: 'Coachee ratings over time'
          },
          xAxis: {
            categories: sortedCategories
          },
          yAxis: {
            allowDecimal: false,
            categories: ['Below Target', 'On Target', 'High Performance', 'Exceptional Performance'],
            min: 0,
            max: 3,
            title: {
              text: ''
            }
          },
          plotOptions: {
            line: {
              dataLabels: {
                enabled: false
              },
              enableMouseTracking: false
            }
          },
          series: series
        };
      });
    });
  }
}
