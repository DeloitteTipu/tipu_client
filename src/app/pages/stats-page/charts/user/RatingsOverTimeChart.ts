import {SessionDataService} from '../../../../service/session-data.service';

export class RatingsOverTimeChart {

  constructor(private sessionData: SessionDataService) {}

  options;

  build() {
    new Promise(resolve => {
      this.sessionData.tipuAPI.getRatingsOverTime(this.sessionData.targetUser.id).subscribe(data => {
        resolve(data);
      });
    }).then((data: {categories: string[], data: number[]}) => {
      this.options = {
        chart: {
          type: 'line',
        },
        credits: {
          enabled: false
        },
        title: {
          text: 'Rating Over Time'
        },
        xAxis: {
          categories: data.categories
        },
        yAxis: {
          allowDecimal: false,
          categories: ['Below Target', 'On Target', 'High Performance', 'Exceptional Performance'],
          min: 0,
          max: 3,
          title: {
            text: ''
          }
        },
        plotOptions: {
          line: {
            dataLabels: {
              enabled: false
            },
            enableMouseTracking: false
          }
        },
        series: [{
          name: 'Your Rating',
          data: data.data
        }]
      };
    });
  }
}
