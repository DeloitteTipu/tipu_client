import {SessionDataService} from '../../../../service/session-data.service';

export class GoalsPieChart {

  constructor(private sessionData: SessionDataService) {}

  options;

  build() {
    new Promise(resolve => {
      this.sessionData.tipuAPI.getGoalsCompletion(this.sessionData.currentUser.id).subscribe(data => {
        resolve(data);
      });
    }).then(data => {
      this.options = {
        chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
        },
        credits: {
          enabled: false
        },
        title: {
          text: 'Goal Completion'
        },
        subtitle: {
          text: 'For All Goals'
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: false
            },
            showInLegend: true
          }
        },
        series: [{
          name: 'Completion',
          colorByPoint: true,
          data: data
        }]
      };
    });
  }
}
