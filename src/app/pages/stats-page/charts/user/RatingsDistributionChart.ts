import {SessionDataService} from '../../../../service/session-data.service';

export class RatingsDistributionChart {

  constructor(private sessionData: SessionDataService) {}

  options;

  build() {
    const year = 2018;
    const eoy = 0;
    new Promise<[{name: string, y: number}]>(resolve => {
      this.sessionData.tipuAPI.getAverageRating(year, eoy).subscribe((data: [{name: string, y: number}]) => {
        resolve(data);
      });
    }).then(data => {

      let total = 0;

      for (const group of data) {
        total += group.y;
      }

      for (const group of data) {
        group.y = Math.round(group.y / total * 100);
      }

      this.options = {
        chart: {
          type: 'column'
        },
        credits: {
          enabled: false
        },
        title: {
          text: 'Distribution Of Ratings'
        },
        subtitle: {
          text: 'All Users For ' + (eoy === 0 ? 'Mid Year' : 'End of Year') + ' Reviews of ' + year
        },
        xAxis: {
          type: 'category'
        },
        yAxis: {
          title: {
            text: 'Percentage of people'
          }

        },
        legend: {
          enabled: false
        },
        plotOptions: {
          series: {
            borderWidth: 0,
            dataLabels: {
              enabled: true,
              format: '{point.y}%'
            }
          }
        },

        series: [
          {
            credits: {
              enabled: false
            },
            name: 'People',
            colorByPoint: true,
            data: data

          }
        ]
      };
    });
  }
}
