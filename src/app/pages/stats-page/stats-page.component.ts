import { Component, OnInit } from '@angular/core';
import {User} from '../../structs/User';
import {SessionDataService} from '../../service/session-data.service';
import {Subscription} from 'rxjs/Subscription';
import * as Highcharts from 'highcharts';
import {RatingsDistributionChart} from './charts/user/RatingsDistributionChart';
import {GoalsPieChart} from './charts/user/GoalsPieChart';
import {RatingsOverTimeChart} from './charts/user/RatingsOverTimeChart';
import {CoacheeRatingBar} from './charts/coach/CoacheeRatingBar';
import {CoacheeGoalsBar} from './charts/coach/CoacheeGoalsBar';
import {CoacheeRatingsOverTimeChart} from './charts/coach/CoacheeRatingsOverTimeChart';
import {Router} from '@angular/router';

@Component({
  selector: 'app-stats-page',
  templateUrl: './stats-page.component.html',
  styleUrls: ['./stats-page.component.scss']
})
export class StatsPageComponent implements OnInit {

  currentUser: User;
  currentUserSubscription: Subscription;

  // targetUser: User;
  // targetUserSubscription: Subscription;

  coachView = false;

  Highcharts = Highcharts;

  barChart = new RatingsDistributionChart(this.sessionData);
  overTime = new RatingsOverTimeChart(this.sessionData);
  pieChart = new GoalsPieChart(this.sessionData);

  coacheeOverTime = new CoacheeRatingsOverTimeChart(this.sessionData);
  coacheeBar = new CoacheeRatingBar(this.sessionData);
  coacheeGoals = new CoacheeGoalsBar(this.sessionData);

  showCoacheeTab: boolean;

  constructor(private sessionData: SessionDataService, private router: Router) {

    if (!this.sessionData.currentUser) {
      this.router.navigate(['/login']);
    }

    /*
    Add subscriptions to the user and goal observables in the service to recieve updates when the user car
    is updated
     */
    this.currentUser = this.sessionData.currentUser;
    this.currentUserSubscription = this.sessionData.currentUserObservable().subscribe((data) => {
      this.currentUser = data;

      this.sessionData.tipuAPI.getUsers({coach_id: data.id}).subscribe(coachees => {
        console.log(coachees.length);
        this.showCoacheeTab = coachees.length > 0;
        console.log(this.showCoacheeTab);
      });
    });

  //   this.targetUser = this.sessionData.targetUser;
  //   this.targetUserSubscription = this.sessionData.targetUserObservable().subscribe((data) => {
  //     this.targetUser = data;
  //   });
  //
  //   // This line is weird but if I dont update the target user then it doesn't seem to bind properly
    this.sessionData.synchronise();
  //
  }


  ngOnInit() {
    this.barChart.build();
    this.overTime.build();
    this.pieChart.build();

    this.coacheeGoals.build();
    this.coacheeBar.build();
    this.coacheeOverTime.build();
  }

}
