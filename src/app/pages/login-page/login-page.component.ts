import { Component, OnInit } from '@angular/core';
import {SessionDataService} from '../../service/session-data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  constructor(private sessionData: SessionDataService, private router: Router) { }

  ngOnInit() {
    // this.sessionData.setCurrentUser(16);
  }

  login(userID: any) {
    // console.log(userID);
    this.sessionData.setCurrentUser(userID).then(() => {
      this.router.navigate(['/reviews']);
    });
  }

}
