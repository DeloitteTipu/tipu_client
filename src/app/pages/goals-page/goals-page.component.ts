import {Component, ElementRef, OnDestroy, OnInit, Renderer2, TemplateRef, ViewChild} from '@angular/core';
import {SessionDataService} from '../../service/session-data.service';
import {User} from '../../structs/User';
import {Subscription} from 'rxjs/Subscription';
import {Goal} from '../../structs/Goal';
import {CommentTarget} from '../../structs/CommentTarget';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Router} from '@angular/router';

@Component({
  selector: 'app-goals-page',
  templateUrl: './goals-page.component.html',
  styleUrls: ['./goals-page.component.scss']
})
export class GoalsPageComponent implements OnInit, OnDestroy {

  /*
  Store the subscribed user and goal variables and subscription for unsubscription in ngOnDestroy
   */
  currentUser: User;
  currentUserSubscription: Subscription;

  targetUser: User;
  targetUserSubscription: Subscription;

  goals: Goal[];
  goalsSubscription: Subscription;

  expandedGoals: Set<number> = new Set<number>();

  includeDate = false;
  pickerDate = new Date();

  updateIncludeDate = false;
  updatePickerDate = new Date();

  commentType = CommentTarget.goal;

  modalRef: BsModalRef;

  constructor(private sessionData: SessionDataService, private modalService: BsModalService, private router: Router) {

    if (!this.sessionData.currentUser) {
      this.router.navigate(['/login']);
    }

    /*
    Add subscriptions to the user and goal observables in the service to recieve updates when the user car
    is updated
     */
    this.currentUser = this.sessionData.currentUser;
    this.currentUserSubscription = this.sessionData.currentUserObservable().subscribe((data) => {
      this.currentUser = data;
    });

    this.targetUser = this.sessionData.targetUser;
    this.targetUserSubscription = this.sessionData.targetUserObservable().subscribe((data) => {
      this.expandedGoals = new Set<number>();
      this.targetUser = data;
    });

    this.goals = this.sessionData.goals;
    this.goalsSubscription = this.sessionData.goalsObservable().subscribe((data) => {
      this.goals = data;
    });

    // This line is weird but if I dont update the target user then it doesn't seem to bind properly
    this.sessionData.updateTargetUser(this.targetUser);

  }

  ngOnInit() {

  }

  ngOnDestroy() {
    /*
    Remove the subscriptions to the service
     */
    this.targetUserSubscription.unsubscribe();
    this.currentUserSubscription.unsubscribe();
    this.goalsSubscription.unsubscribe();
  }

  toggleGoal(id: number) {
    if (this.expandedGoals.has(id)) {
      this.closeGoal(id);
    } else {
      this.openGoal(id);
    }
  }

  openGoal(id: number) {
    // this.expandedGoals.clear();
    this.expandedGoals = new Set<number>().add(id);
  }

  closeGoal(id: number) {
    this.expandedGoals.delete(id);
  }

  createGoal(title: string, description: string) {

    if (!title || title === '') {
      return;
    }

    const details = {
      user_id: this.targetUser.id,
      setter_id: this.sessionData.currentUser.id,
      title: title,
      description: description,
      last_update: this.formatDateForSQL(new Date())
    };

    if (this.includeDate) {
      details['end_date'] = this.formatDateForSQL(this.pickerDate);
    }

    this.sessionData.tipuAPI.createGoal(
      details
    ).subscribe((goalList) => {

      this.sessionData.updateGoals(this.goals.concat([goalList[0]]));
      this.openGoal(goalList[0].id);
    });
  }

  updateDate(event) {
    this.includeDate = event['open'];
    this.pickerDate = event['date'];
  }

  updateUpdateDate(event) {
    this.updateIncludeDate = event['open'];
    this.updatePickerDate = new Date(event['date']);
  }

  updateGoal(goalID: number, newTitle: string, newDescription: string, newDate: Date) {
    console.log(newDate);
    this.sessionData.tipuAPI.updateGoal(goalID,
      {
        title: newTitle,
        description: newDescription,
        end_date: this.formatDateForSQL(newDate),
        last_update: (new Date ((new Date((new Date(new Date())).toISOString() )).getTime() - ((new Date())
          .getTimezoneOffset() * 60000))).toISOString().slice(0, 19).replace('T', ' ')
      })
      .subscribe((updatedGoalList) => {
        this.sessionData.synchronise();
        this.closeModal();
    });
  }

  updateGoalCompletion(goalID: number, complete: boolean) {

    this.sessionData.tipuAPI.updateGoal(goalID,
      {complete: complete,
        date_finished: complete ? this.formatDateForSQL(new Date()) : null})
      .subscribe(() => {
      this.sessionData.synchronise();
    });
  }

  updateGoalModal(template: TemplateRef<any>, goalID: number) {
    this.modalRef = this.modalService.show(template);
  }

  confirmDeleteGoal(template: TemplateRef<any>, goalID: number) {
    this.modalRef = this.modalService.show(template);
  }

  deleteGoal(goalID: number) {
    this.sessionData.tipuAPI.deleteGoal(goalID).subscribe((data) => {
      if (data['deleted'] === true) {
        this.sessionData.synchronise();
        this.closeModal();
      } else {
        console.log('Couldn\'t delete this goal');
      }
    });
  }

  closeModal() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  formatDateForSQL(date: Date) {
    return date ? (new Date ((new Date((new Date(date)).toISOString() )).getTime() - ((new Date()).getTimezoneOffset() * 60000)))
      .toISOString().slice(0, 19).replace('T', ' ') : null;
  }

  isInFuture(date: Date) {
    return new Date(date) > new Date();
  }



}
