import { Pipe, PipeTransform } from '@angular/core';
import {TipuAPIService} from '../service/tipuapi.service';

@Pipe({
  name: 'tipuUserFullName'
})
export class TipuUserFullNamePipe implements PipeTransform {

  constructor( private tipuAPI: TipuAPIService) {

  }

  transform(userID: number, args?: any): any {

    const promise = new Promise((resolve, reject) => {
      this.tipuAPI.getUsers({id: userID}).subscribe((userList) => {
        if (userList[0] !== undefined) {
          resolve(userList[0].first_name + ' ' + userList[0].last_name);
        }
      });
    });

    return promise;
  }

}
