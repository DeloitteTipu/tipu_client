import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ratingString'
})
export class RatingStringPipe implements PipeTransform {

  ranks = {0: 'Below Target', 1: 'On Target', 2: 'High Performance', 3: 'Exceptional Performance'};

  transform(value: number, args?: any): any {
    return this.ranks[value];
  }

}
