import { TestBed, inject } from '@angular/core/testing';

import { TipuAPIService } from './tipuapi.service';

describe('TipuAPIService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipuAPIService]
    });
  });

  it('should be created', inject([TipuAPIService], (service: TipuAPIService) => {
    expect(service).toBeTruthy();
  }));
});
