import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {User} from '../structs/User';
import {Goal} from '../structs/Goal';
import {Review} from '../structs/Review';
import {Deletion} from '../structs/Deletion';
import {CommentTarget} from '../structs/CommentTarget';
import {Comment} from '../structs/Comment';
import {Host} from './hosts';


@Injectable()
export class TipuAPIService {

  apiVersion = 'v1';
  host = Host.deployment;
  urlRoot = this.host + '/api/' + this.apiVersion;

  userEndpoint = this.urlRoot + '/user/';
  goalEndpoint = this.urlRoot + '/goal/';
  reviewEndpoint = this.urlRoot + '/review/';
  commentEndpoint = this.urlRoot + '/:target_type/:target_id/comment/';
  statsEndpoint = this.urlRoot + '/stats/:stat_type';

  constructor(private http: HttpClient) { }

  /**
   * ======================================================================
   * BELOW HERE IS THE METHODS FOR THE USER ENDPOINT INTERACTION
   * ======================================================================
   */

  /**
   * This method will return all users in the database. It optionally accepts a dictionary of filter options,
   * all of which are optional. Only values set will be used
   * @returns {Observable<User[]>} A list of User objects that matched the filter conditions
   * @param filter Optional filter options to sort users
   */
  getUsers(filter?: {id?: number, first_name?: string, last_name?: string, coach_id?: number, role?: string, email?: string}): Observable<User[]> {

    let params = new HttpParams();

    if (filter) {
      Object.keys(filter).forEach((key) => {
        params = params.append(key, filter[key]);
      });
    }

    return this.http.get<User[]>(this.userEndpoint, {params: params});
  }

  /**
   * This method accepts a dictionary of values that describe a user and saves the values in to the database, returning the User object
   * in a list where it is the only element
   * @returns {Observable<User[]>} Observable list of Users where the new user is the only item
   * @param details Combination of required and optional values that describe a user
   */
  createUser(details: {first_name: string, last_name: string, coach_id?: number, role?: string, email?: string}): Observable<User[]> {
    return this.http.post<User[]>(this.userEndpoint, details);
  }

  /**
   * This method accepts a dictionary of values that describe changes that should be made to a user in the database
   * @param {number} id Targets the user to be edited by ID
   * @param updates A dictionary of changes to be made. Should have at least 1 item.
   * @returns {Observable<User[]>} Observable containing a list of users where the updated user is the only element
   */
  updateUser(id: number, updates: {
    id?: number, first_name?: string, last_name?: string, coach_id?: number, role?: string, email?: string, photo_url?: string
  }): Observable<User[]> {

    return this.http.put<User[]>(this.userEndpoint + id, updates);
  }

  /**
   * This method will delete a user from the database
   *
   * IT SHOULD BE REMOVED IF IT IS NOT NEEDED
   *
   * @param {number} id The ID of the user to be deleted
   * @returns {Observable<Deletion>} Deletion object which has a boolean on if the deletion was successful
   */
  deleteUser(id: number): Observable<Deletion> {
    return this.http.delete<Deletion>(this.userEndpoint + id);
  }

  /**
   * ======================================================================
   * BELOW HERE IS THE METHODS FOR THE GOAL ENDPOINT INTERACTION
   * ======================================================================
   */

  /**
   * This method will return all goals in the database. It optionally accepts a dictionary of filter options,
   * all of which are optional. Only values set will be used
   * @returns {Observable<User[]>} A list of User objects that matched the filter conditions
   * @param filter Optional filter options to sort goals
   */
  getGoals(filter?: {user_id?: number, id?: number, setter_id?: number, goal_id?: number}): Observable<Goal[]> {

    let params = new HttpParams();

    if (filter) {
      Object.keys(filter).forEach((key) => {
        params = params.append(key, filter[key]);
      });
    }

    return this.http.get<Goal[]>(this.goalEndpoint, {params: params});
  }

  /**
   * This method accepts a dictionary of values that describe a goal and saves the values in to the database, returning the User object
   * in a list where it is the only element
   * @returns {Observable<Goal[]>} Observable list of Goals where the new goal is the only item
   * @param details Combination of required and optional values that describe a goal
   */
  createGoal(details: {user_id: number, setter_id: number, description: string, title: string,
    date_made?: string, date_finished?: string, end_date?: string, progress?: number, complete?: boolean, last_update?: string}): Observable<Goal[]> {

    return this.http.post<Goal[]>(this.goalEndpoint, details);
  }

  /**
   * This method accepts a dictionary of values that describe changes that should be made to a goal in the database
   * @param {number} id Targets the goal to be edited by ID
   * @param updates A dictionary of changes to be made. Should have at least 1 item.
   * @returns {Observable<Goal[]>} Observable containing a list of goals where the updated goal is the only element
   */
  updateGoal(id: number, updates: {
    id?: number, goal_id?: number, setter_id?: number, description?: string, title?: string,
    date_made?: string, date_finished?: string, end_date?: string, progress?: number, complete?: boolean, last_update?: string
  }): Observable<Goal[]> {

    return this.http.put<Goal[]>(this.goalEndpoint + id, updates);
  }

  /**
   * This method will delete a goal from the database
   *
   * IT SHOULD BE REMOVED IF IT IS NOT NEEDED
   *
   * @param {number} id The ID of the goal to be deleted
   * @returns {Observable<Deletion>} Deletion object which has a boolean on if the deletion was successful
   */
  deleteGoal(id: number): Observable<Deletion> {
    return this.http.delete<Deletion>(this.goalEndpoint + id);
  }

  /**
   * ======================================================================
   * BELOW HERE IS THE METHODS FOR THE REVIEW ENDPOINT INTERACTION
   * ======================================================================
   */

  /**
   * This method will return all reviews in the database. It optionally accepts a dictionary of filter options,
   * all of which are optional. Only values set will be used
   * @returns {Observable<User[]>} A list of User objects that matched the filter conditions
   * @param filter Optional filter options to sort reviews
   */
  getReviews(filter?: {user_id?: number,
    year?: number,
    end_of_year?: number,
    user_comment?: string,
    coach_comment?: string,
    promotion_milestone?: string,
    user_rating?: number,
    coach_rating?: number,
    coach_id?: number}): Observable<Review[]> {

    let params = new HttpParams();

    if (filter) {
      Object.keys(filter).forEach((key) => {
        params = params.append(key, filter[key]);
      });
    }

    return this.http.get<Review[]>(this.reviewEndpoint, {params: params});
  }

  /**
   * This method accepts a dictionary of values that describe a review and saves the values in to the database, returning the User object
   * in a list where it is the only element
   * @returns {Observable<Review[]>} Observable list of Reviews where the new review is the only item
   * @param details Combination of required and optional values that describe a review
   */
  createReview(details: {
    user_id: number,
    year: number,
    end_of_year: number,
    user_comment?: string,
    coach_comment?: string,
    promotion_milestone?: string,
    user_rating?: number,
    coach_rating?: number,
    coach_id: number}): Observable<Review[]> {

    return this.http.post<Review[]>(this.reviewEndpoint, details);
  }

  /**
   * This method accepts a dictionary of values that describe changes that should be made to a review in the database
   * @param {number} id Targets the review to be edited by ID
   * @param updates A dictionary of changes to be made. Should have at least 1 item.
   * @returns {Observable<Review[]>} Observable containing a list of reviews where the updated review is the only element
   */
  updateReview(id: number, updates: {
    id?: number,
    user_id?: number,
    year?: number,
    end_of_year?: number,
    user_comment?: string,
    coach_comment?: string,
    promotion_milestone?: string,
    user_rating?: number,
    coach_rating?: number,
    coach_id?: number
  }): Observable<Review[]> {

    return this.http.put<Review[]>(this.reviewEndpoint + id, updates);
  }

  /**
   * This method will delete a review from the database
   *
   * IT SHOULD BE REMOVED IF IT IS NOT NEEDED
   *
   * @param {number} id The ID of the review to be deleted
   * @returns {Observable<Deletion>} Deletion object which has a boolean on if the deletion was successful
   */
  deleteReview(id: number): Observable<Deletion> {
    return this.http.delete<Deletion>(this.reviewEndpoint + id);
  }


  /**
   * ======================================================================
   * BELOW HERE IS THE METHODS FOR THE COMMENT ENDPOINT INTERACTION
   * ======================================================================
   */

  /**
   * This method will return all comments in the database. It optionally accepts a dictionary of filter options,
   * all of which are optional. Only values set will be used
   * @returns {Observable<User[]>} A list of User objects that matched the filter conditions
   * @param target
   * @param target_id
   * @param filter Optional filter options to sort comments
   */
  getComments(target: CommentTarget, target_id: number, filter?: {id?: number, user_id?: number, title?: string, content?: string,
    commenter_id?: number, rating?: number, quarter?: number, year?: number}): Observable<Comment[]> {

    let params = new HttpParams();

    if (filter) {
      Object.keys(filter).forEach((key) => {
        params = params.append(key, filter[key]);
      });
    }

    return this.http.get<Comment[]>(this.commentEndpoint.replace(':target_type', target).replace(':target_id', String(target_id)),
      {params: params});
  }

  /**
   * This method accepts a dictionary of values that describe a comment and saves the values in to the database, returning the User object
   * in a list where it is the only element
   * @returns {Observable<Comment[]>} Observable list of Comments where the new comment is the only item
   * @param target
   * @param target_id
   * @param details Combination of required and optional values that describe a comment
   */
  createComment(target: CommentTarget, target_id: number, details: {commenter_id: number,
    content: string, replying_id?: number}): Observable<Comment[]> {

    console.log(this.commentEndpoint.replace(':target_type', target).replace(':target_id', String(target_id)));

    return this.http.post<Comment[]>(
      this.commentEndpoint.replace(':target_type', target).replace(':target_id', String(target_id)), details);
  }

  /**
   * This method accepts a dictionary of values that describe changes that should be made to a comment in the database
   * @param target
   * @param target_id
   * @param comment_id
   * @param updates A dictionary of changes to be made. Should have at least 1 item.
   * @returns {Observable<Comment[]>} Observable containing a list of comments where the updated comment is the only element
   */
  updateComment(target: CommentTarget, target_id: number, comment_id: number, updates: {
    id?: number, user_id?: number, title?: string, content?: string,
    commenter_id?: number, rating?: number, quarter?: number, year?: number
  }): Observable<Comment[]> {

    return this.http.put<Comment[]>(
      this.commentEndpoint.replace(':target_type', target).replace(':target_id', String(target_id)) + comment_id, updates);
  }

  /**
   * This method will delete a comment from the database
   *
   * IT SHOULD BE REMOVED IF IT IS NOT NEEDED
   *
   * @param target
   * @param target_id
   * @param {number} comment_id The ID of the comment to be deleted
   * @returns {Observable<Deletion>} Deletion object which has a boolean on if the deletion was successful
   */
  deleteComment(target: CommentTarget, target_id: number, comment_id: number): Observable<Deletion> {
    return this.http.delete<Deletion>(
      this.commentEndpoint.replace(':target_type', target).replace(':target_id', String(target_id)) + comment_id);
  }

  /**
   * ======================================================================
   * BELOW HERE IS THE METHODS FOR THE STATISTICS ENDPOINT INTERACTION
   * ======================================================================
   */


  getAverageRating(year: number, end_of_year: number, coach_id?: number): Observable<[{name: string, y: number}]> {
    let params = new HttpParams();

    params = params.append('year', String(year));
    params = params.append('end_of_year', String(end_of_year));
    if (coach_id) {
      params = params.append('coach_id', String(coach_id));
    }

    return this.http.get<[{name: string, y: number}]>(
      this.statsEndpoint.replace(':stat_type', 'average_rating'), {params: params});
  }

  getRatingsOverTime(user_id: number): Observable<{categories: string[], data: number[]}> {
    let params = new HttpParams();
    params = params.append('user_id', String(user_id));

    return this.http.get<{categories: string[], data: number[]}>(
      this.statsEndpoint.replace(':stat_type', 'rating_over_time'),
      {params: params});
  }

  getGoalsCompletion(user_id: number): Observable<[{name: string, y: number}]> {
    let params = new HttpParams();
    params = params.append('user_id', String(user_id));

    return this.http.get<[{name: string, y: number}]>(
      this.statsEndpoint.replace(':stat_type', 'goal_completion'), {params: params});
  }

  getUserImageURL(urlSlug: string) {
    return this.host + '/api/images/' + (urlSlug !== null ? urlSlug : 'default.png');
  }

}
