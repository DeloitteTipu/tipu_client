export enum Host {
  deployment = 'http://www.tipu.xyz',
  localhost = 'http://localhost:8080'
}
