import { Injectable } from '@angular/core';
import {User} from '../structs/User';
import {Subject} from 'rxjs/Subject';
import {TipuAPIService} from './tipuapi.service';
import {Goal} from '../structs/Goal';
import {Review} from '../structs/Review';

@Injectable()
export class SessionDataService {

  targetUserSubject: Subject<User> = new Subject<User>();
  currentUserSubject: Subject<User> = new Subject<User>();
  goalsSubject: Subject<Goal[]> = new Subject<Goal[]>();
  reviewsSubject: Subject<Review[]> = new Subject<Review[]>();

  targetUser: User;
  currentUser: User;
  goals: Goal[];
  reviews: Review[];


  constructor(public tipuAPI: TipuAPIService) {
    // this.setCurrentUser(16);
  }

  setCurrentUser(userID: number): Promise<null> {
    return new Promise(resolve => {
      const promises = [];
      // The ID below is the "authenticated user" that the system will load for
      promises.push(new Promise(flag => {
        this.tipuAPI.getUsers({id: userID}).subscribe((user) => {
          // Once we have their user data assign that data as both the current and target user as we start focused
          // on the user themselves
          this.updateCurrentUser(user[0]);
          this.updateTargetUser(user[0]);

          flag();

        });
      }));

      // Add a subscription to the targetUser so that when it changes the goals stored here will update to match
      promises.push(new Promise(flag => {
        this.targetUserObservable().subscribe((targetUser) => {
          this.tipuAPI.getGoals({user_id: targetUser.id}).subscribe((goals) => {
            this.updateGoals(goals);
            flag();
          });
        });
      }));


      promises.push(new Promise(flag => {
        this.targetUserObservable().subscribe((targetUser) => {
          this.tipuAPI.getReviews({user_id: targetUser.id}).subscribe((reviews) => {
            this.updateReviews(reviews);
            flag();
          });
        });
      }));

      Promise.all(promises).then(() => {
        resolve();
      });
    });

  }

  /**
   * This will pull the latest data from the server including goals and reviews so it is useful
   * after items have been edited or deleted
   */
  synchronise() {
    this.updateTargetUser(this.targetUser);
    this.updateCurrentUser(this.currentUser);
  }

  targetUserObservable() {
    return this.targetUserSubject.asObservable();
  }

  updateTargetUser(user: User) {
    if (user) {
      this.targetUserSubject.next(user);
      this.targetUser = user;
    }
  }

  currentUserObservable() {
    return this.currentUserSubject.asObservable();
  }

  updateCurrentUser(user: User) {
    if (user) {
      this.currentUserSubject.next(user);
      this.currentUser = user;
    }
  }

  updateGoals(goals: Goal[]) {
    this.goalsSubject.next(goals);
  }

  goalsObservable() {
    return this.goalsSubject.asObservable();
  }

  updateReviews(reviews: Review[]) {
    this.reviewsSubject.next(reviews);
  }

  reviewsObservable() {
    return this.reviewsSubject.asObservable();
  }

}
