import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import {TipuAPIService} from './service/tipuapi.service';
import {HttpClientModule} from '@angular/common/http';
import {UserCardComponent} from './shared/user-card/user-card.component';
import {FormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import { GoalsPageComponent } from './pages/goals-page/goals-page.component';
import { StatsPageComponent } from './pages/stats-page/stats-page.component';
import { ReviewsPageComponent } from './pages/reviews-page/reviews-page.component';
import {SessionDataService} from './service/session-data.service';
import { NavigationComponent } from './shared/navigation/navigation.component';
import { ClickOutsideModule } from 'ng-click-outside';
import {SuiModule} from 'ng2-semantic-ui';
import {MomentModule} from 'ngx-moment';
import { TipuUserFullNamePipe } from './pipes/tipu-user-full-name.pipe';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {NgDatepickerModule} from 'ng2-datepicker';
import { DatePickerButtonComponent } from './shared/date-picker-button/date-picker-button.component';
import { CommentThreadComponent } from './shared/comment-thread/comment-thread.component';
import { CommentComponent } from './shared/comment-thread/comment/comment.component';
import {ModalModule} from 'ngx-bootstrap';
import { RatingSliderComponent } from './shared/rating-slider/rating-slider.component';
import { TipuUserFirstNamePipe } from './pipes/tipu-user-first-name.pipe';
import {HighchartsChartModule} from 'highcharts-angular';
import {RatingStringPipe} from './pipes/rating-string.pipe';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { AlertModule } from 'ngx-bootstrap/alert';


const appRoutes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginPageComponent },
  { path: 'goals', component: GoalsPageComponent },
  { path: 'reviews', component: ReviewsPageComponent },
  { path: 'stats', component: StatsPageComponent }

  ];

@NgModule({
  declarations: [
    AppComponent,
    UserCardComponent,
    GoalsPageComponent,
    StatsPageComponent,
    ReviewsPageComponent,
    NavigationComponent,
    TipuUserFullNamePipe,
    DatePickerButtonComponent,
    CommentThreadComponent,
    CommentComponent,
    RatingSliderComponent,
    TipuUserFirstNamePipe,
    RatingStringPipe,
    LoginPageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    ClickOutsideModule,
    SuiModule,
    MomentModule,
    BsDatepickerModule.forRoot(),
    NgDatepickerModule,
    HighchartsChartModule,
    ModalModule.forRoot(),
    RouterModule.forRoot(
      appRoutes
    )

  ],
  providers: [TipuAPIService, SessionDataService],
  bootstrap: [AppComponent]
})

export class AppModule { }
