import { Component, OnInit } from '@angular/core';
import {TipuAPIService} from './service/tipuapi.service';
import {CommentTarget} from './structs/CommentTarget';
import {User} from './structs/User';
import * as $ from 'jquery';
import {SessionDataService} from './service/session-data.service';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private sessionData: SessionDataService, private router: Router) {}



  // currentUser: Observable<User> = this.sessionData.currentUserObservable();
  // targetUser: Observable<User> = this.sessionData.targetUserObservable();
  //
  updateUser(user: User) {
    this.sessionData.updateTargetUser(user);
  }

  getURL(): string {
    return this.router.url;
  }

  /*
  This starts the app with the user set to user ID 7
   */
  ngOnInit() {

  }
}
